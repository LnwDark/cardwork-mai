<?php

require __DIR__ . '/vendor/autoload.php';
include_once("./CardWorkDraw.php");

// error_reporting(E_ALL);

/* Parameter */
$print = new stdClass();

$print->code = "12908";
$print->name = "นางสาวกิตติยาพร เชี่ยวชาญสุทธิกุล";
$print->name_EN = "Ms.Kittiyaporn Sutti Suttakul";
$print->image = "profile/59200.jpg";
$print->positions ="ผู้อำนวยการสถานศึกษา";
$print->subjects = "ศิลปะ ดนตรีและนาฎศิลป์sdf";
//$print->name ="นายปีเตอร์ อเล็กซานเดอร์  ซาอัดอิบราฮิมออธแมน";
//$print->name = "นายพล โจ้";

//โรงเรียนหัวหินวิทยาคม (ครู)
$templateName = 'teacher-huahin';
$draw = new CardWorkDraw($templateName, $print->code);
$draw->drawProfile($x = 88,$y = 271,$width = 195, '' . $print->image);
$draw->drawTextCenter($print->name, $y= 334, $fontSize = 55,300,0,$color='#000000');
$draw->drawText($print->positions, $x = 510, $y = 410,$fontSize = 52);
$draw->drawText($print->subjects, $x = 565, $y = 485,$fontSize = 52);
$draw->show();

