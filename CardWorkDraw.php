<?php

//use Imagick;
//use ImagickDraw;
//use ImagickPixel;
use Picqer\Barcode\BarcodeGeneratorPNG;

class CardWorkDraw {
    public $image;
    public $position;
    private $templateName;
    private $code;
    public $fontBold;
    public $fontNormal;


    public function __construct($templateName, $code)
    {
        $this->image = new Imagick();
        $this->templateName = $templateName;
        $this->code = $code;
        $path = './template' . DIRECTORY_SEPARATOR . $templateName . '_front.jpg';
        $this->fontBold = "./assets/THSarabunNew.ttf";
        $this->fontNormal = "./assets/THSarabunNew.ttf";
        // $this->fontBold = "./assets/ThaiSansNeue-ExtraBold.ttf";
        // $this->fontNormal = "./assets/ThaiSansNeue-Regular.ttf";
        $handle = fopen($path, 'rb');
        $this->image->readImageFile($handle);
    }

    public function drawTextCenter($text, $y, $fontSize, $offsetLeft = 0, $offsetRight = 0, $color = '#000000') {

        $imageWidth = $this->image->getImageWidth() - $offsetLeft - $offsetRight;

        $color = new ImagickPixel($color);
        $draw = new ImagickDraw($color);
        $draw->setFont($this->fontBold);
        $draw->setFontSize($fontSize);
        $draw->setFillColor($color);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);
        $draw->setTextKerning(-1);
        /* Get font metrics */
        $metrics = $this->image->queryFontMetrics($draw, $text);
        $textWidth = $metrics['textWidth'];
        $x = ($imageWidth - $textWidth)/2 + $offsetLeft;

        if ($textWidth > $imageWidth) {
            return $this->drawTextCenter($text, $y, $fontSize-1, $offsetLeft, $offsetRight, $color);
        }

      // $draw->annotation($x, $y+$metrics['ascender'], $text);
        $draw->annotation($x, $y, $text);
        $this->image->drawImage($draw);
    }

    public function drawText($text, $x, $y, $fontSize, $maxWidth = null, $fontType = null, $color = '#000000') {

        if (is_null($fontType)) $fontType = $this->fontBold;

        $color = new ImagickPixel($color);
        $draw = new ImagickDraw();
        $draw->setFont($fontType);
        $draw->setFontSize($fontSize);
        $draw->setFillColor($color);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);
        $draw->setTextKerning(-1);
        /* Get font metrics */
        $metrics = $this->image->queryFontMetrics($draw, $text);
        /* Create text */
        $this->imageWidth = $this->image->getImageWidth();
        $textWidth = $metrics['textWidth'];

        if (is_numeric($maxWidth) && $textWidth > $maxWidth) {
            return $this->drawText($text,$x,$y, $fontSize-1, $maxWidth, $fontType);
        }

        $draw->annotation($x, $y, $text);
        $this->image->drawImage($draw);
    }

    public function drawProfile($x, $y, $width, $url) {
        $image2 = new Imagick();
        $handle = fopen($url, 'rb');
        $image2->readImageFile($handle);
        $image2->resizeImage($width, $width/3*4, Imagick::DISPOSE_NONE, 1);
        $this->image->compositeImage($image2, $image2->getImageCompose(), $x, $y);
    }

    public function drawBarcode($text, $x, $y, $width, $height, $vertical = false , $opacity, $roundCorner = false ) {

        if ($opacity > 0) {
            $draw = new ImagickDraw();
            $draw->setFillColor('white');
            $draw->setStrokeWidth(2);
            $draw->setFillOpacity($opacity);
            $corner = $roundCorner ? 10 : 0;
            $draw->roundRectangle( $x-5, $y-5, $x+$width+5, $y+$height+5 ,$corner,$corner);
            $this->image->drawImage( $draw );
        }

        $generator = new BarcodeGeneratorPNG();
        $barcode = $generator
            ->getBarcode($text, $generator::TYPE_CODE_39,3, ($width > $height ? $height : $width ));

        $image2 = new Imagick();
        $image2->readimageblob($barcode);
        if ($vertical) $image2->rotateImage(new ImagickPixel('#00000000'), 90);
        $image2->resizeImage($width, $height, Imagick::DISPOSE_NONE, 1);

        $this->image->compositeImage($image2, $image2->getImageCompose(), $x, $y);
    }

    public function save() {
        $path = \Yii::getAlias('@backend') . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "images"
            .DIRECTORY_SEPARATOR;
        if (!file_exists($path)) mkdir($path, 0777, true);
        $imagePath = $path . $this->templateName . '_' . $this->code. ".png";
        $this->image->writeImage($imagePath);
    }

    public function show() {
        header('Content-type: image/png');
        echo $this->image;
        exit();
    }

    public function formatCitizenID($citizen_id, $spacer) {
        $c = $citizen_id;
        if (strlen($c) == 13) {
            return substr($c,0,1).$spacer.substr($c,1,4).$spacer.substr($c,5,5).$spacer.substr($c,10,2).$spacer.$c[12];
        }

        return $citizen_id;
    }

}
